import React from 'react';
import { StyleSheet, FlatList, } from 'react-native';
import { List, } from 'react-native-elements';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';

import { constants, } from '../styles';
import { SONG_ROW_HEIGHT} from '../constants';
import { selectSong, } from '../actions';
import SongsListItem from '../components/SongsListItem';

class SongsList extends React.PureComponent {

    constructor(props) {
        super(props);
        console.log('SongsList ---> constructor');
        this.renderItem = this.renderItem.bind(this);
        this.onPressSong = this.onPressSong.bind(this);
        this.getItemLayout = this.getItemLayout.bind(this);
    }
    getItemLayout = (data, index) => {
        return {length: SONG_ROW_HEIGHT, offset: SONG_ROW_HEIGHT * index, index}
    }
    renderItem = ({ item }) => {
        return (
            <SongsListItem item={item} onPressSong={this.onPressSong}/>
        );
    };
    render() {
        console.log("%c --SongsList render--", "background: yellow; color: black;");

        return (
            <List containerStyle={styles.list}>
                <FlatList
                    renderItem={this.renderItem}
                    data={this.props.filteredSongs}
                    keyExtractor={item => item._id}
                    style={styles.list}
                    getItemLayout={this.getItemLayout}
                />
            </List>
        );
    }
    onPressSong = (song) => {
        this.props.selectSong(song);
    };
}

const mapStateToProps = (state) => ({
    filteredSongs: state.data.filteredSongs,
});
const mapDispatchToProps = dispatch => ({
    selectSong: (sng) => dispatch(selectSong(sng)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SongsList);


const styles = StyleSheet.create({
    list: {
        marginTop: 0,
        backgroundColor: constants.dark_2,
        borderTopWidth: 0,
        borderBottomWidth: 0,
        flex: 1
    },
});