import React from 'react';
import { ScrollView, StyleSheet, View, } from 'react-native';
import { Avatar, } from 'react-native-elements';
import { connect } from 'react-redux';

import { constants, commonStyles } from '../styles';
import Lyrics from './Lyrics';
import FontSlider from './FontSlider';
import { changeArtist, } from '../actions';

class LyricsScreen extends React.Component { //React.PureComponent
    static navigationOptions = ({ navigation, screenProps }) => ({
        title: navigation.state.params.name_sinhala,
        headerStyle: commonStyles.headerStyle,
        headerTitleStyle: commonStyles.headerTitleStyle
    });
   
    constructor(props) {
        super(props);
        console.log('LyricsScreen ---> constructor');
        this.onPressArtist = this.onPressArtist.bind(this);
    }
    shouldComponentUpdate(nextProps, nextState) {
        console.log("%c --LyricsScreen should update--", "background: yellow; color: black;");
        return false;
    }
    render() {
        console.log("%c --LyricsScreen render--", "background: yellow; color: black;");
        return (
            <View style={styles.container}>
                <ScrollView contentContainerStyle={styles.textContainer}>
                    <Lyrics lyrics={ this.props.sng.lyrics }/>
                    <View style={styles.artistContainer}>
                        {
                            this.props.sng.artists.map((artist) => (
                                <Avatar
                                    key={artist._id}
                                    medium 
                                    source={{ uri: artist.base64 }}
                                    activeOpacity={0.7}
                                    containerStyle={styles.avatar}
                                    onPress={() => this.onPressArtist(artist)}
                                />
                            ))
                        }
                    </View>
                </ScrollView>
                <FontSlider />
            </View>
        );
    }
    onPressArtist = (artist) => {
        this.props.changeArtist(artist);
    };
}
const mapStateToProps = (state) => ({
    sng: state.data.selectedSong
});
const mapDispatchToProps = dispatch => ({
    changeArtist: (artist) => dispatch(changeArtist(artist)),
});
export default connect(mapStateToProps, mapDispatchToProps)(LyricsScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: constants.dark_2,
        justifyContent: 'space-between',
    },
    textContainer: {
        flex: 0,
        padding: 5,
        justifyContent: 'space-between'
    },
    sliderContainer: {
        flexDirection: 'row',
        height: 40,
        marginLeft: 10,
        marginRight: 10,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    lbl: {
        color: constants.dark_5,
        fontSize: 15,
        width: 80,
    },
    slider: {
        flex: 1,
    },
    pin: {
        backgroundColor: constants.blue
    },
    artistContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 30
    },
    avatar: {
        marginLeft: 2,
        marginRight: 2,
    }
});
