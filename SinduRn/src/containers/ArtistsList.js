import React from 'react';
import { StyleSheet, FlatList, } from 'react-native';
import { List, } from 'react-native-elements';
import { connect } from 'react-redux';

import { constants, } from '../styles';
import { ARTIST_ROW_HEIGHT} from '../constants';
import { selectedArtist, } from '../actions';
import ArtistsListItem from '../components/ArtistsListItem';

class ArtistsList extends React.PureComponent {

    constructor(props) {
        super(props);
        console.log('ArtistsList ---> constructor');
        this.renderItem = this.renderItem.bind(this);
        this.onPressArtist = this.onPressArtist.bind(this);
        this.getItemLayout = this.getItemLayout.bind(this);
    }
    getItemLayout = (data, index) => {
        return {length: ARTIST_ROW_HEIGHT, offset: ARTIST_ROW_HEIGHT * index, index}
    }
    renderItem = ({ item }) => {
        return <ArtistsListItem item={item} onPressArtist={this.onPressArtist}/>;
    };
    render() {
        console.log("%c --ArtistsList render--", "background: yellow; color: black;");

        return (
            <List containerStyle={styles.list}>
                <FlatList
                    renderItem={this.renderItem}
                    data={this.props.filteredArtists}
                    keyExtractor={item => item._id}
                    style={styles.list}
                    getItemLayout={this.getItemLayout}
                />
            </List>
        );
    }
    onPressArtist = (artist) => {
        this.props.selectArtist(artist);
    };
}

const mapStateToProps = (state) => ({
    filteredArtists: state.data.filteredArtists,
});
const mapDispatchToProps = dispatch => ({
    selectArtist: (artist) => dispatch(selectedArtist(artist)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ArtistsList);


const styles = StyleSheet.create({
    list: {
        marginTop: 0,
        backgroundColor: constants.dark_2,
        borderTopWidth: 0,
        borderBottomWidth: 0,
        flex: 1
    },
});