import React from 'react';
import { Text, } from 'react-native';
import { connect } from 'react-redux';

class Lyrics extends React.Component {
    constructor(props) {
        super(props);
    }
    shouldComponentUpdate(nextProps, nextState) {
        console.log("%c --Lyrics should update--", "background: yellow; color: black;");
        return true;
    }
    render() {
        console.log("%c --Lyrics render--", "background: yellow; color: black;");

        return (
            <Text style={{ color: 'white', fontSize: this.props.fontSize, }}> 
                {this.props.lyrics}
            </Text>
        );
    }
}
const mapStateToProps = (state) => ({
    fontSize: state.data.fontSize
});
export default connect(mapStateToProps)(Lyrics);
