import React from 'react';
import { StyleSheet, } from 'react-native';
import { SearchBar, } from 'react-native-elements';
import { connect } from 'react-redux';

import { constants, } from '../styles';
import { filterSongs } from '../actions';

class SongsSeachbar extends React.Component {

    constructor(props) {
        super(props);
        console.log('SongsSeachbar ---> constructor');
    }
    shouldComponentUpdate(nextProps, nextState) {
        console.log("%c --SongsSeachbar should update--", "background: yellow; color: black;");
        return true;
    }

    render() {
        console.log("%c --SongsSeachbar render--", "background: yellow; color: black;");

        return (
            <SearchBar
                ref={search => this.search = search}
                round
                autoCorrect={false}
                icon={{ name: 'search' }}
                clearIcon={{ name: 'clear' }}
                placeholder='Search...'
                onChangeText={this.onChangeText}
                // onClearText={this.onClearText}
                containerStyle={styles.searchbar}
                value={this.props.songsSearchText}
            />
        );
    }
    onChangeText = (a) => {
        const newVal = a.toLowerCase().trim(' ');
        if (newVal !== this.props.songsSearchText){
            this.props.filterSongs(newVal);
        }
    };
}

const mapStateToProps = (state) => ({
    songsSearchText: state.data.songsSearchText,
});
const mapDispatchToProps = dispatch => ({
    filterSongs: (val) => dispatch(filterSongs(val)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SongsSeachbar);

const styles = StyleSheet.create({
    searchbar: {
        borderTopWidth: 0,
        borderBottomWidth: 0,
        backgroundColor: constants.dark_1,
    },
});

