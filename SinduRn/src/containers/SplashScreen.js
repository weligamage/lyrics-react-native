import React from 'react';
import { ImageBackground, StyleSheet, ActivityIndicator, View, Text, } from 'react-native';
import { connect } from 'react-redux';
import { Button } from 'react-native-elements';

import { constants, commonStyles } from '../styles';
import bgImage from "../../assets/img/splash.png";
import { initData } from '../actions';

class SplashScreen extends React.Component { //React.PureComponent
    static navigationOptions = {
        title: 'ගී පද',
        headerStyle: commonStyles.headerStyle,
        headerTitleStyle: commonStyles.headerTitleStyle
    };

    constructor(props) {
        super(props);
        console.log('SplashScreen ---> constructor');
    }
    componentDidMount() {
        console.log('did mount >>>>>>>>>', this.props);
        this.props.initData();
    }
    shouldComponentUpdate(nextProps, nextState) {
        console.log("%c --SplashScreen should update--", "background: yellow; color: black;");
        if (this.props.isError !== nextProps.isError){
            return true;
        }
        return false;
    }
    onClickRetry = () => {
        this.props.initData();
    }
    render() {
        console.log("%c --SplashScreen render--", "background: yellow; color: black;");

        return (
            <ImageBackground style={styles.container} source={bgImage}>
                {
                    this.props.isError ?
                        <View style={styles.textContainer}>
                            <Text style={styles.problem}>HAVING A COMMUNICATION PROBLEM ?</Text>
                            <Button onPress={this.onClickRetry} 
                                title="Retry" 
                                textStyle={styles.btnLabel}
                                buttonStyle={styles.btn}
                            />
                        </View>
                        : <View style={styles.textContainer}>
                            <ActivityIndicator size="large" color='white' />
                            <Text style={styles.plswait}>PLEASE WAIT...</Text>
                            <Text style={styles.weare}>We are preparing your app...</Text>
                        </View>
                }
            </ImageBackground>
        );
    }
}

const mapStateToProps = (state) => ({
    isError: state.splash.isError
});
const mapDispatchToProps = dispatch => ({
    initData: () => dispatch(initData()),
});
export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    textContainer: {
        marginBottom: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    plswait: {
        marginTop: 50,
        color: 'white',
        fontSize: 16,
        fontFamily: "Ubuntu",
        // fontWeight: '200',
    },
    weare: {
        color: 'white',
        fontSize: 12,
        paddingTop: 10,
        fontFamily: "Ubuntu",
        // fontWeight: '200',
    },
    problem: {
        color: 'white',
        fontSize: 15,
        fontFamily: "Ubuntu",
        marginBottom: 20
    },
    btn: {
        backgroundColor: "rgba(0, 0, 0, .1)",
        borderColor: "white",
        borderWidth: 1,
        borderRadius: 22,
        width: 100,
        height: 40
    },
    btnLabel: {
        color: 'white',
        fontFamily: "Ubuntu",
    }
    
});

// the connect method transforms the current Redux store state and imported actions into the props you want to pass to a presentational component 

//component is inserted into DOM
// constructor()
// componentWillMount()
// render()
// componentDidMount()

//props or state is changed
// componentWillReceiveProps()
// shouldComponentUpdate(nextProps, nextState)
// componentWillUpdate()
// render()
// componentDidUpdate()

//component is removed from the DOM
// componentWillUnmount()

//each instance has properties ( props/ state), class properties (defaultProps/ displayName), methods (setState, forceUpdate)

//shouldComponentUpdate
//dont' declare defaults on each render, instead declare it top and use it
// functions the same, dont' create function identifiers for each render, bind the function on constructor instead
// ex:

// class App extends PureComponent {
//     constructor(props) {
//         super(props);
//         this.update = this.update.bind(this);
//     }
//     update(e) {
//         this.props.update(e.target.value);
//     }
//     render() {
//         return <MyInput onChange={this.update} />;
//     }
// }

// map state functions should be written with reselect library

// let App = ({ otherData, resolution }) => (
//     <div>
//         <DataContainer data={otherData} />
//         <ResolutionContainer resolution={resolution} />
//     </div>
// );
// const doubleRes = (size) => ({
//     width: size.width * 2,
//     height: size.height * 2
// });
// App = connect(state => {
//     return {
//         otherData: state.otherData,
//         resolution: doubleRes(state.resolution)
//     }
// })(App);

// This is because the doubleRes function will always return a new resolution object with a new identity. 

// Reselect memoizes the last result of the function and returns it when called until new arguments are passed to it.
// import {createSelector} from “reselect”;
// const doubleRes = createSelector(
//   r => r.width,
//   r => r.height,
//   (width, height) => ({
//     width: width*2,
//     height: heiht*2
//   })
// );


// The difference between them is that React.Component doesn’t implement shouldComponentUpdate(), but React.PureComponent implements it with a shallow prop and state comparison.

//use immutable library , https://facebook.github.io/immutable-js/



//myths https://moduscreate.com/blog/react_component_rendering_performance/   comparison