import React from 'react';
import { StyleSheet, Text, View, } from 'react-native';
import { Slider, } from 'react-native-elements';
import { connect } from 'react-redux';

import { constants, } from '../styles';
import { changeFontSize } from '../actions';

class FontSlider extends React.Component {
    constructor(props) {
        super(props);
    }
    shouldComponentUpdate(nextProps, nextState) {
        console.log("%c --FontSlider should update--", "background: yellow; color: black;");
        return false;
    }
    render() {
        console.log("%c --FontSlider render--", "background: yellow; color: black;");
        return (
            <View style={styles.sliderContainer}>
                <Text style={styles.lbl}>Font Size</Text>
                <Slider
                    value={this.props.fontSize}
                    onValueChange={(value) => {
                        if (value !== this.props.fontSize) {
                            this.props.changeFontSize(value);
                        }
                    }}
                    maximumValue={25}
                    minimumValue={16}
                    step={1}
                    style={styles.slider}
                    thumbStyle={styles.pin}
                    minimumTrackTintColor={constants.dark_5}
                    maximumTrackTintColor={constants.dark_3}
                />
            </View>
        );
    }
}
const mapStateToProps = (state) => ({
    fontSize: state.data.fontSize
});
const mapDispatchToProps = dispatch => ({
    changeFontSize: (size) => dispatch(changeFontSize(size)),
});
export default connect(mapStateToProps, mapDispatchToProps)(FontSlider);

const styles = StyleSheet.create({
    sliderContainer: {
        flexDirection: 'row',
        height: 40,
        marginLeft: 10,
        marginRight: 10,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    lbl: {
        color: constants.dark_5,
        fontSize: 15,
        width: 80,
    },
    slider: {
        flex: 1,
    },
    pin: {
        backgroundColor: constants.blue
    },
});
