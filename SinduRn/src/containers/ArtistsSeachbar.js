import React from 'react';
import { StyleSheet, } from 'react-native';
import { SearchBar, } from 'react-native-elements';
import { connect } from 'react-redux';

import { constants, } from '../styles';
import { filterArtists } from '../actions';

class ArtistsSeachbar extends React.Component {

    constructor() {
        super();
        console.log('ArtistsSeachbar ---> constructor');
    }
    shouldComponentUpdate(nextProps, nextState) {
        console.log("%c --ArtistsSeachbar should update--", "background: yellow; color: black;");
        return true;
    }

    render() {
        console.log("%c --ArtistsSeachbar render--", "background: yellow; color: black;");

        return (
            <SearchBar
                ref={search => this.search = search}
                round
                autoCorrect={false}
                icon={{ name: 'search' }}
                clearIcon={{ name: 'clear' }}
                placeholder='Search...'
                onChangeText={this.onChangeText}
                // onClearText={this.onClearText}
                containerStyle={styles.searchbar}
                value={this.props.artistsSearchText}
            />
        );
    }
    onChangeText = (a) => {
        const newVal = a.toLowerCase().trim(' ');
        if (newVal !== this.props.artistsSearchText){
            this.props.filterArtists(newVal);
        }
    };
}

const mapStateToProps = (state) => ({
    artistsSearchText: state.data.artistsSearchText,
});
const mapDispatchToProps = dispatch => ({
    filterArtists: (val) => dispatch(filterArtists(val)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ArtistsSeachbar);

const styles = StyleSheet.create({
    searchbar: {
        borderTopWidth: 0,
        borderBottomWidth: 0,
        backgroundColor: constants.dark_1,
    },
});

