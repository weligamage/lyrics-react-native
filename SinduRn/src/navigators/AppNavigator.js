import React from 'react';
import { StackNavigator, TabNavigator, } from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import { Avatar, } from 'react-native-elements';

import ArtistTab from '../components/ArtistTab';
import SongsTab from '../components/SongsTab';
import FavouritesTab from '../components/FavouritesTab';
import SplashScreen from '../containers/SplashScreen';
import LyricsScreen from '../containers/LyricsScreen';
import { constants, } from '../styles';

const TabNav = TabNavigator(
    {
        ArtistsTab: {
            screen: ArtistTab,
        },
        SongsTab: {
            screen: SongsTab,
        },
        FavouritesTab: {
            screen: FavouritesTab
        }
    },
    {
        lazy: true,
        tabBarPosition: 'bottom',
        swipeEnabled: false,
        animationEnabled: false,
        tabBarOptions: {
            showLabel: false,
            showIcon: true,
            activeTintColor: constants.blue, //lbl and icon color
            activeBackgroundColor: constants.dark_4,
            inactiveTintColor: constants.dark_6,
            inactiveBackgroundColor: constants.dark_2,
            style: {
                backgroundColor: constants.dark_6
            }
        },
        navigationOptions: ({ navigation, screenProps }) => ({
            header: null,
            tabBarIcon: ({ focused, tintColor }) => {
                const { routeName } = navigation.state;
                if (routeName === 'ArtistsTab') {
                    return <Icon name={focused ? 'ios-person' : 'ios-person-outline'} size={constants.tabIconSize} color={tintColor} />;
                } else if (routeName === 'SongsTab') {
                    if (screenProps && screenProps.base64){
                        return <Avatar
                            small
                            source={{ uri: screenProps.base64, cache: 'force-cache' }}
                        />;
                    }else{
                        return <Icon name={focused ? "ios-musical-notes" : "ios-musical-notes-outline"} size={constants.tabIconSize} color={tintColor} />;
                    }
                }else{
                    return <Icon name={focused ? "ios-star" : "ios-star-outline"} size={constants.tabIconSize} color={tintColor} />;
                }
            },
        }),
    }
);

const AppNavigator = StackNavigator(
    {
        Splash: {
            screen: SplashScreen
        },
        Home: {
            screen: TabNav,
        },
        Lyrics: {
            screen: LyricsScreen
        },
    },
    {
        headerMode: 'screen',
        initialRouteName: 'Splash',
        navigationOptions: {
            gesturesEnabled: false
        }
    }
);

export default AppNavigator;