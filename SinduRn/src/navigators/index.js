import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addNavigationHelpers, } from 'react-navigation';
import { View } from 'react-native';

import { addListener } from '../utils/redux';
import AppNavigator from './AppNavigator';
import { commonStyles } from '../styles';

class AppWithNavigationState extends React.Component {
    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        nav: PropTypes.object.isRequired,
    };

    render() {
        const { dispatch, nav } = this.props;
        return (
            <View style={commonStyles.container}>

                <AppNavigator
                    navigation={addNavigationHelpers({
                        dispatch,
                        state: nav,
                        addListener,
                    })} 
                    screenProps={this.props.selectedArtist}
                />
            </View>
        );
    }
}

const mapStateToProps = state => ({
    nav: state.nav,
    selectedArtist: state.data.selectedArtist,
});

export default connect(mapStateToProps)(AppWithNavigationState);
// so we will have navigation.state, navigation.dispatch, navigation.addListener