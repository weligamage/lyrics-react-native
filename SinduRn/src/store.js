import { createStore, applyMiddleware } from 'redux';
import { createLogger} from 'redux-logger';
import promiseMiddleware from 'redux-promise-middleware';
import thunk from 'redux-thunk';

import AppReducer from './reducers';
import { navReduxMiddleware } from './utils/redux';

const middleware = [navReduxMiddleware, createLogger(), promiseMiddleware(), thunk];
const store = createStore(
    AppReducer,
    applyMiddleware(...middleware),
);

export default store;
