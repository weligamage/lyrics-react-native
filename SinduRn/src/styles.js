import { StyleSheet } from 'react-native';

const constants = {
    dark_1: "#262d36",
    dark_2: "#1f242a",
    dark_3: "#2f3640",
    dark_4: "#2f3640",
    dark_5: "#cbcbcd",
    dark_6: '#4f5b6b', 

    blue: '#177efb',
    tabIconSize: 26,
};

const commonStyles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerStyle: {
        backgroundColor: constants.dark_1,
    },
    headerTitleStyle: {
        color: 'white',
        // fontFamily: 'WARNA',
        // fontSize: 22
    }
});

export { commonStyles, constants }; 