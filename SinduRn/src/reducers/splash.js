import {
    INIT_DATA_PENDING,
    INIT_DATA_FULFILLED,
    INIT_DATA_REJECTED,
} from '../constants'

const initialState = {
    isAppInitialized: false,
    isAppInitializing: false,
    isError: false
};

export default function splashReducer(state = initialState, action) {

    console.log("%c splashReducer: " + action.type, "background: blue; color: white;", action.payload);

    switch (action.type) {
        case INIT_DATA_PENDING:
            return {
                ...state,
                isAppInitializing: true,
                isError: false,
            };
        case INIT_DATA_REJECTED:
            return {
                ...state,
                isAppInitializing: false,
                isError: true
            };
        case INIT_DATA_FULFILLED:
            return {
                ...state,
                isAppInitializing: false,
                isAppInitialized: true,
            };
        default:
            return state;
    }
};
