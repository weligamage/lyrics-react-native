import {
    FETCH_ARTISTS_PENDING,
    FETCH_ARTISTS_FULFILLED,
    FETCH_ARTISTS_REJECTED,

    FETCH_SONGS_PENDING,
    FETCH_SONGS_FULFILLED,
    FETCH_SONGS_REJECTED,

    INIT_DATA_FULFILLED,
    UPDATE_DATA,

    FILTER_ARTISTS,
    FILTER_SONGS,
    SELECT_ARTIST,
    SELECT_SONG,
    NAVIGATION,
    CHANGE_FONT_SIZE,
    CHANGE_ARTIST,

} from '../constants'

const initialState = {
    artists: [],
    artistsSearchText: '',
    filteredArtists: [],
    isArtistsFetched: false,
    isArtistsFetching: false,
    songs: [],
    songsSearchText: '',
    filteredSongs: [],
    isSongsFetched: false,
    isSongsFetching: false,
    selectedArtist: null,
    songsByArtists: [],
    isError: false,
    errorMsg: null,
    selectedSong: null,
    fontSize: 16,
};

export default function dataReducer(state = initialState, action) {
    console.log("%c dataReducer: " + action.type, "background: blue; color: white;", action.payload);

    switch (action.type) {
        case INIT_DATA_FULFILLED:
            return {
                ...state,
                artists: action.payload.artists,
                songs: action.payload.songs,
                filteredArtists: action.payload.artists,
                filteredSongs: action.payload.songs,
            };
        case UPDATE_DATA:
            if (state.selectedArtist) {
                const songsWithArtists = [];
                for (let i = 0; i < action.payload.songs.length; i++) {
                    for (let j = 0; j < action.payload.songs[i].artists.length; j++) {
                        if (action.payload.songs[i].artists[j] === state.selectedArtist._id) {
                            songsWithArtists.push(action.payload.songs[i]);
                            break;
                        }
                    }
                }
                return {
                    ...state,
                    artists: action.payload.artists,
                    songs: action.payload.songs,
                    filteredArtists: (state.artistsSearchText === '') ? action.payload.artists : action.payload.artists.filter(({ search }) => {
                        return search.match(state.artistsSearchText);
                    }),
                    songsByArtists: songsWithArtists,
                    filteredSongs: (state.songsSearchText === '') ? songsWithArtists : songsWithArtists.filter(({ search }) => {
                        return search.match(state.songsSearchText);
                    }),
                };
            } else {
                return {
                    ...state,
                    artists: action.payload.artists,
                    songs: action.payload.songs,
                    filteredArtists: (state.artistsSearchText === '') ? action.payload.artists : action.payload.artists.filter(({ search }) => {
                        return search.match(state.artistsSearchText);
                    }),
                    filteredSongs: (state.songsSearchText === '') ? action.payload.songs : action.payload.songs.filter(({ search }) => {
                        return search.match(state.songsSearchText);
                    }),
                };
            }
        case FILTER_ARTISTS:
            return {
                ...state,
                artistsSearchText: action.payload,
                filteredArtists: state.artists.filter(({ search }) => {
                    return search.match(action.payload);
                }),
            };
        case FILTER_SONGS:
            return {
                ...state,
                songsSearchText: action.payload,
                filteredSongs: state[state.selectedArtist ? 'songsByArtists' : 'songs'].filter(({ search }) => {
                    return search.match(action.payload);
                }),
            };
        case CHANGE_ARTIST:
        case SELECT_ARTIST:
            if (!state.selectedArtist || (state.selectedArtist._id !== action.payload._id)) {
                const songsWithArtists = [];
                for (let i = 0; i < state.songs.length; i++) {
                    for (let j = 0; j < state.songs[i].artists.length; j++) {
                        if (state.songs[i].artists[j] === action.payload._id) {
                            songsWithArtists.push(state.songs[i]);
                            break;
                        }
                    }
                }
                return {
                    ...state,
                    selectedArtist: action.payload,
                    songsByArtists: songsWithArtists,
                    filteredSongs: songsWithArtists.filter(({ search }) => {
                        return search.match(state.songsSearchText);
                    }),
                };
            }else{
                return state;
            }
        case NAVIGATION:
            if (action.routeName === 'ArtistsTab' && state.selectedArtist !== null){
                return {
                    ...state,
                    selectedArtist: null,
                    filteredSongs: (state.songsSearchText === '') ? state.songs : state.songs.filter(({ search }) => {
                        return search.match(state.songsSearchText);
                    }),
                };
            }
            return state;
        case SELECT_SONG:
            const sng = Object.assign({}, action.payload);
            let splitArr = sng.lyrics.split("@");
            sng.lyrics = splitArr.join("\n");
            let artList = [];
            for(let i = 0; i < sng.artists.length; i++){
                for (let j = 0; j < state.artists.length; j++){
                    if (sng.artists[i] === state.artists[j]._id){
                        artList.push(Object.assign({}, state.artists[j]));
                        break;
                    }
                }
            }
            sng.artists = artList;
            return {
                ...state,
                selectedSong: sng,
            };
        case CHANGE_FONT_SIZE:  
            return {
                ...state,
                fontSize: action.payload,
            }; 
        default:
            return state;
    }
};
