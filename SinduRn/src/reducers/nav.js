import { NavigationActions } from 'react-navigation';

import AppNavigator from '../navigators/AppNavigator';

import {
    INIT_DATA_FULFILLED,
    SELECT_ARTIST,
    SELECT_SONG,
    CHANGE_ARTIST,

} from '../constants'

const firstAction = AppNavigator.router.getActionForPathAndParams('Splash');
const firstNavState = AppNavigator.router.getStateForAction(firstAction);

const navReducer = (state = firstNavState, action) => {
    console.log("%c navReducer: " + action.type, "background: blue; color: white;", action.payload);
    let nextState;
    switch (action.type) {
        case INIT_DATA_FULFILLED:
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({ routeName: 'ArtistsTab' }),
                state
            );
            break;
        case SELECT_ARTIST:
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({ routeName: 'SongsTab'}),
                state
            );
            break;
        case SELECT_SONG:
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({ routeName: 'Lyrics', params: action.payload }),
                state
            );
            break;
        case CHANGE_ARTIST:
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.back(),
                state
            );
            break;
        default:
            nextState = AppNavigator.router.getStateForAction(action, state);
            break;
    }

    // Simply return the original `state` if `nextState` is null or undefined.
    return nextState || state;
};

export default navReducer;