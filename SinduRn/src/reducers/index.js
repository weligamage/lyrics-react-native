import { combineReducers } from 'redux';

import nav from './nav';
import splash from './splash';
import data from './data';

const AppReducer = combineReducers({
    splash,
    data,
    nav,
});

export default AppReducer;