import React from 'react';
import { View, Text, StyleSheet, } from 'react-native';

import { constants, commonStyles } from '../styles';
import ColorStatusBar from './ColorStatusBar';

class FavouritesTab extends React.Component {
    
    constructor(props) {
        super(props);
        console.log('FavouritesTab ---> constructor');
    }
    shouldComponentUpdate(nextProps, nextState) {
        console.log("%c --FavouritesTab should update--", "background: yellow; color: black;");
        return false;
    }
    render() {
        console.log("%c --FavouritesTab render--", "background: yellow; color: black;");

        return (
            <View style={styles.container}>
                <ColorStatusBar />
                <Text>Fav Tab</Text>
            </View>
        );
    }
}

export default FavouritesTab;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: constants.dark_2
    },
    searchbar: {
        borderTopWidth: 0,
        borderBottomWidth: 0,
        backgroundColor: constants.dark_1,
    },
    list: {
        marginTop: 0,
        backgroundColor: constants.dark_2,
        borderTopWidth: 0,
        borderBottomWidth: 0,
        marginBottom: 22,
        flex: 1
    },
    listItem: {
        borderBottomColor: constants.dark_3,
    },
    sinhala: {
        // fontSize: 24,
        // fontWeight: 'bold',
        color: 'white',
        // fontFamily: 'WARNA'
    },
    english: {
        // backgroundColor: 'yellow',
        // alignItems: 'center',
        // justifyContent: 'center',
        textAlign: 'right',
        fontSize: 12,
        fontWeight: '200',
    },
});