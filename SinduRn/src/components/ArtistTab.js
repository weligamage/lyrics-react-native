import React from 'react';
import { View, StyleSheet, } from 'react-native';

import { constants, } from '../styles';
import ColorStatusBar from './ColorStatusBar';
import ArtistsSeachbar from '../containers/ArtistsSeachbar';
import ArtistsList from '../containers/ArtistsList';

class ArtistsTab extends React.PureComponent {
    render() {
        console.log("%c --ArtistsTab render--", "background: yellow; color: black;");

        return (
            <View style={styles.container}>
                <ColorStatusBar />
                <ArtistsSeachbar />
                <ArtistsList />
            </View>
        );
    }
}

export default ArtistsTab;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: constants.dark_2
    },
});