import React from 'react';
import { View, StyleSheet, } from 'react-native';

import { constants,  } from '../styles';
import ColorStatusBar from './ColorStatusBar';
import SongsSeachbar from '../containers/SongsSeachbar';
import SongsList from '../containers/SongsList';

class SongsTab extends React.Component {

    constructor(props) {
        super(props);
        console.log('SongsTab ---> constructor');
    }
    shouldComponentUpdate(nextProps, nextState) {
        console.log("%c --SongsTab should update--", "background: yellow; color: black;");
        return false;
    }
    render() {
        console.log("%c --SongsTab render--", "background: yellow; color: black;");
        return (
            <View style={styles.container}>
                <ColorStatusBar />
                <SongsSeachbar/>
                <SongsList />
            </View>
        );
    }
}

export default SongsTab;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: constants.dark_2
    },
    avatar: {
        backgroundColor: constants.dark_2,
    }
});