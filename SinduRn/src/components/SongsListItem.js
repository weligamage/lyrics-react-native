import React from 'react';
import { StyleSheet, } from 'react-native';
import { ListItem, Avatar, } from 'react-native-elements';

import { constants, } from '../styles';

class SongsListItem extends React.PureComponent {

    onPressSong = () => {
        this.props.onPressSong(this.props.item);
    }
    render() {
        console.log("%c --SongsListItem render--", "background: yellow; color: black;", this.props);

        return (
            <ListItem
                avatar={<Avatar
                    medium
                    icon={{ name: 'ios-musical-notes', color: constants.blue, type: 'ionicon' }}
                    activeOpacity={0.7}
                    overlayContainerStyle={styles.avatar}
                />}
                onPress={this.onPressSong}
                title={this.props.item.name_sinhala}
                titleStyle={styles.sinhala}
                subtitle={this.props.item.name_english}
                subtitleStyle={styles.english}
                containerStyle={styles.listItem}
                underlayColor={constants.dark_1} 
            />
        );
    }
}

export default SongsListItem;


const styles = StyleSheet.create({
    listItem: {
        borderBottomColor: constants.dark_3,
    },
    sinhala: {
        color: 'white',
    },
    english: {
        textAlign: 'right',
        fontSize: 12,
        fontWeight: '200',
    },
    avatar:{
        backgroundColor: constants.dark_2,
    }
});