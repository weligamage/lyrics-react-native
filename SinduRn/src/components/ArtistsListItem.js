import React from 'react';
import { StyleSheet, } from 'react-native';
import { ListItem, Avatar, } from 'react-native-elements';

import { constants, } from '../styles';

class ArtistsListItem extends React.PureComponent {

    onPressArtist = () => {
        this.props.onPressArtist(this.props.item);
    }
    render() {
        console.log("%c --ArtistsList render--", "background: yellow; color: black;", this.props);

        return (
            <ListItem
                avatar={<Avatar
                    medium
                    source={{ uri: this.props.item.base64, cache: 'force-cache' }}
                />}
                onPress={this.onPressArtist}
                title={this.props.item.name_sinhala}
                titleStyle={styles.sinhala}
                subtitle={this.props.item.name_english}
                subtitleStyle={styles.english}
                containerStyle={styles.listItem}
                underlayColor={constants.dark_1} 
            />
        );
    }
}

export default ArtistsListItem;

const styles = StyleSheet.create({
    listItem: {
        borderBottomColor: constants.dark_3,
    },
    sinhala: {
        color: 'white',
    },
    english: {
        textAlign: 'right',
        fontSize: 12,
        fontWeight: '200',
    },
});