import React from 'react';
import { View, StyleSheet, StatusBar, Platform } from 'react-native';

import { constants, } from '../styles';

class ColorStatusBar extends React.PureComponent {

    render() {
        console.log("%c --ColorStatusBar render--", "background: yellow; color: black;");

        return (
            <View style={styles.statusbar} >
                <StatusBar barStyle="light-content" />
            </View>
        );
    }
}

export default ColorStatusBar;

const statusbarHeight = (Platform.OS === 'ios') ? 20 : StatusBar.currentHeight;

const styles = StyleSheet.create({
    statusbar: {
        backgroundColor: constants.dark_2,
        height: statusbarHeight
    }
});

