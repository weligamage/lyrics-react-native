import axios from 'axios';
import { AsyncStorage } from 'react-native';

import {
    INIT_DATA,
    INIT_DATA_PENDING,
    INIT_DATA_REJECTED,
    INIT_DATA_FULFILLED,
    UPDATE_DATA,
    API_URI_VERIFIED_ARTISTS,
    API_URI_VERIFIED_SONGS,
    KEY_ARTISTS,
    KEY_SONGS,
    FETCH_ARTISTS_PENDING,
    FETCH_ARTISTS_FULFILLED,
    FETCH_ARTISTS_REJECTED,
    FETCH_SONGS_PENDING,
    FETCH_SONGS_FULFILLED,
    FETCH_SONGS_REJECTED,
    FILTER_ARTISTS,
    FILTER_SONGS,
    SELECT_ARTIST,
    SELECT_SONG,
    CHANGE_FONT_SIZE,
    CHANGE_ARTIST,
} from './constants';

function getArtists() {
    return new Promise((resolve, reject) => {
        console.log('get artists...');
        axios.get(API_URI_VERIFIED_ARTISTS)
            .then((response) => {
                console.log("%c --success API_URI_VERIFIED_ARTISTS--", "background: green; color: white;", response);
                if (response.data && response.data instanceof Array && response.data.length > 0) {
                    AsyncStorage.setItem(KEY_ARTISTS, JSON.stringify(response.data));
                    return resolve(response.data);
                } else {
                    return reject("artists_empty");
                }
            })
            .catch((err) => {
                console.log("%c --fail API_URI_VERIFIED_ARTISTS--", "background: red; color: white;", err);
                return reject("artists_failed");
            });
    });
}
function getSongs() {
    return new Promise((resolve, reject) => {
        console.log('get songs...');
        axios.get(API_URI_VERIFIED_SONGS)
            .then((response) => {
                console.log("%c --success API_URI_VERIFIED_SONGS--", "background: green; color: white;", response);
                if (response.data && response.data instanceof Array && response.data.length > 0) {
                    AsyncStorage.setItem(KEY_SONGS, JSON.stringify(response.data));
                    return resolve(response.data);
                } else {
                    return reject("songs_empty");
                }
            })
            .catch((err) => {
                console.log("%c --fail API_URI_VERIFIED_SONGS--", "background: red; color: white;", err);
                return reject("songs_failed");
            });
    });
}
function updateCache() {
    return new Promise((resolve, reject) => {
        getArtists()
            .then(artists => {
                getSongs()
                    .then(songs => {
                        return resolve({
                            artists,
                            songs
                        });
                    })
                    .catch(sngErr => {
                        return reject(sngErr);
                    });
            })
            .catch(artErr => {
                return reject(artErr);
            });
    });
}
function initCache() {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(KEY_ARTISTS).then(artists => {
            if (artists) {
                artists = JSON.parse(artists);
                console.log("%c KEY_ARTISTS", "background: green; color: white;", artists);
                AsyncStorage.getItem(KEY_SONGS).then(songs => {
                    if (songs) {
                        songs = JSON.parse(songs);
                        console.log("%c KEY_SONGS", "background: green; color: white;", songs);
                        return resolve({
                            artists,
                            songs
                        });
                    } else {
                        return reject("cache_empty_songs");
                    }
                }).done();
            } else {
                return reject("cache_empty_artists");
            }
        }).done();
    });
}
export function initData() {
    // AsyncStorage.clear();
    return (dispatch) => {
        dispatch({ type: INIT_DATA_PENDING });

        initCache()
            .then(cacheData => {
                dispatch({ type: INIT_DATA_FULFILLED, payload: cacheData });
                updateCache()
                    .then(newData => {
                        dispatch({ type: UPDATE_DATA, payload: newData });
                    })
                    .catch(fetchErr => {
                        // console.log("%c --" + fetchErr + "--", "background: red; color: white;");
                    });
            })
            .catch(cacheErr => {
                updateCache()
                    .then(fetchData => {
                        dispatch({ type: INIT_DATA_FULFILLED, payload: fetchData });
                    })
                    .catch(fetchErr => {
                        dispatch({ type: INIT_DATA_REJECTED });
                    });
            });
    };
}
export function fetchArtists() {
    return (dispatch) => {
        dispatch({ type: FETCH_ARTISTS_PENDING });
        getArtists()
            .then(artists => {
                dispatch({ type: FETCH_ARTISTS_FULFILLED, payload: artists });
            })
            .catch(err => {
                dispatch({ type: FETCH_ARTISTS_REJECTED });
            });
    };
    // return {
    //     type: FETCH_ARTISTS,
    //     payload: axios.get(API_URI_VERIFIED_ARTISTS)
    // };
}
export function fetchSongs() {
    dispatch({ type: FETCH_SONGS_PENDING });
    getSongs()
        .then(songs => {
            dispatch({ type: FETCH_SONGS_FULFILLED, payload: songs });
        })
        .catch(err => {
            dispatch({ type: FETCH_SONGS_REJECTED });
        });
    // return {
    //     type: FETCH_SONGS,
    //     payload: axios.get(API_URI_VERIFIED_SONGS)
    // };
}
export function filterArtists(searchText){
    return {
        type: FILTER_ARTISTS,
        payload: searchText
    };
}
export function filterSongs(searchText){
    return {
        type: FILTER_SONGS,
        payload: searchText
    };
}
export function selectedArtist(artist){
    return {
        type: SELECT_ARTIST,
        payload: artist
    };
}
export function selectSong(sng){
    return {
        type: SELECT_SONG,
        payload: sng
    };
}
export function changeFontSize(val){
    return {
        type: CHANGE_FONT_SIZE,
        payload: val
    };
}
export function changeArtist(artist){
    return {
        type: CHANGE_ARTIST,
        payload: artist
    };
}
