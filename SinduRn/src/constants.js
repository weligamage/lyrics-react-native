export const FETCH_ARTISTS = 'FETCH_ARTISTS';
export const FETCH_ARTISTS_PENDING = 'FETCH_ARTISTS_PENDING';
export const FETCH_ARTISTS_FULFILLED = 'FETCH_ARTISTS_FULFILLED';
export const FETCH_ARTISTS_REJECTED = 'FETCH_ARTISTS_REJECTED';

export const FETCH_SONGS = 'FETCH_SONGS';
export const FETCH_SONGS_PENDING = 'FETCH_SONGS_PENDING';
export const FETCH_SONGS_FULFILLED = 'FETCH_SONGS_FULFILLED';
export const FETCH_SONGS_REJECTED = 'FETCH_SONGS_REJECTED';

export const INIT_DATA = 'INIT_DATA';
export const INIT_DATA_PENDING = 'INIT_DATA_PENDING';
export const INIT_DATA_FULFILLED = 'INIT_DATA_FULFILLED';
export const INIT_DATA_REJECTED = 'INIT_DATA_REJECTED';
export const UPDATE_DATA = 'UPDATE_DATA';

export const BACKEND_URI = "http://nodejs-mongo-persistent-geepada.a3c1.starter-us-west-1.openshiftapps.com";
export const API_URI_VERIFIED_ARTISTS = BACKEND_URI + "/artists/verified";
export const API_URI_VERIFIED_SONGS = BACKEND_URI + "/songs/verified";

export const KEY_ARTISTS = "KEY_ARTISTS";
export const KEY_SONGS = "KEY_SONGS";

export const FILTER_ARTISTS = 'FILTER_ARTISTS';
export const FILTER_SONGS = 'FILTER_SONGS';
export const SELECT_ARTIST = 'SELECT_ARTIST';
export const SELECT_SONG = 'SELECT_SONG';
export const CHANGE_FONT_SIZE = 'CHANGE_FONT_SIZE';
export const CHANGE_ARTIST = 'CHANGE_ARTIST';

export const ARTIST_ROW_HEIGHT = 71;
export const SONG_ROW_HEIGHT = 71;

export const NAVIGATION = 'Navigation/NAVIGATE';