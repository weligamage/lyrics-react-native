1. react-native init SinduRn (dont run yet)
2. react-native-ci-tools bundle "com.rcw.sindulyrics" "SinduLyrics" --ios
2.1. react-native run-ios
3. restart machine - no need
4. add custom fonts / https://medium.com/react-native-training/react-native-custom-fonts-ccc9aacf9e5e
"rnpm": {
    "assets": [
	  "./assets/fonts/"
    ]
  }
react-native link

if u refresh the app there will be errors
4.1 close app, xcode clean, xcode build, after that rn run-ios

5. vector icons / https://github.com/oblador/react-native-vector-icons
npm install react-native-vector-icons --save
npm install
react-native link

* no need for pod , 
if any error occurs delete node_modules/ios/android and do a reset head
npm install
react-native link
reastart the machine

this will build success, but can't use icon imports yet

this will fix it, https://github.com/oblador/react-native-vector-icons/issues/626
"scripts": {
    "postinstall": "rm ./node_modules/react-native/local-cli/core/__fixtures__/files/package.json"
}

import Icon from 'react-native-vector-icons/FontAwesome';
<Text style={styles.welcome}>
    Welcome to React Native!
    <Icon name="rocket" color="#4F8EF7" />
</Text>

6. ok now going to try redux
this is the official site
https://github.com/react-navigation/react-navigation/tree/master/examples/ReduxExample

another example bit old
https://github.com/shubhnik/redux-react-navigation-demos/tree/nestedNavigators




but I already installed navigation package since it doesn't have any linking
npm install --save react-navigation

7. installing axios 

but it doesn't allow our api calls //https://stackoverflow.com/questions/46254088/error-network-error-in-react-native

had to add these to info.plist
<key>NSAppTransportSecurity</key>
	<dict>
		<key>NSAllowsArbitraryLoads</key>
		<true/>
		<key>NSExceptionDomains</key>
		<dict>
			<key>localhost</key>
			<dict>
				<key>NSExceptionAllowsInsecureHTTPLoads</key>
				<true/>
			</dict>
			<key>nodejs-mongo-persistent-geepada.a3c1.starter-us-west-1.openshiftapps.com</key>
			<dict>
				<key>NSExceptionAllowsInsecureHTTPLoads</key>
				<true/>
				<key>NSIncludesSubdomains</key>
				<true/>
			</dict>
		</dict>
	</dict>


//installing redux-promise-middleware  not yet

8. following redux again
https://medium.com/react-native-training/redux-4-ways-95a130da0cdc

> redux-thunk : asynchronous library
returns a function instead of an action, so we can delay the dispatch of the action

> redux promise middleware should be the one use for async operations
it appends _PENDING, _FULFILLED, _REJECTED automatically

but it's easy to use thunk along with it


9. installing react-native elements
https://react-native-training.github.io/react-native-elements/docs/button.html


//  jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
  jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];

// Place this code after "[self.window makeKeyAndVisible]" and before "return YES;"
  UIView* launchScreenView = [[[NSBundle mainBundle] loadNibNamed:@"LaunchScreen" owner:self options:nil] objectAtIndex:0];
  launchScreenView.frame = self.window.bounds;
  rootView.loadingView = launchScreenView;


If you can calculate the height of your rows without rendering them, you can improve the user experience by providing the getItemLayout prop
find_dimesions(layout){
    const {x, y, width, height} = layout;
    console.log(width);
    console.log(height);
}
onLayout={(event) => { this.find_dimesions(event.nativeEvent.layout) }}

//component is inserted into DOM
// constructor()
// componentWillMount()
// render()
// componentDidMount()

//props or state is changed
// componentWillReceiveProps()
// shouldComponentUpdate(nextProps, nextState)
// componentWillUpdate()
// render()
// componentDidUpdate()

//component is removed from the DOM
// componentWillUnmount()

//each instance has properties ( props/ state), class properties (defaultProps/ displayName), methods (setState, forceUpdate)

//shouldComponentUpdate
//dont' declare defaults on each render, instead declare it top and use it
// functions the same, dont' create function identifiers for each render, bind the function on constructor instead
// ex:

// class App extends PureComponent {
//     constructor(props) {
//         super(props);
//         this.update = this.update.bind(this);
//     }
//     update(e) {
//         this.props.update(e.target.value);
//     }
//     render() {
//         return <MyInput onChange={this.update} />;
//     }
// }

// map state functions should be written with reselect library

// let App = ({ otherData, resolution }) => (
//     <div>
//         <DataContainer data={otherData} />
//         <ResolutionContainer resolution={resolution} />
//     </div>
// );
// const doubleRes = (size) => ({
//     width: size.width * 2,
//     height: size.height * 2
// });
// App = connect(state => {
//     return {
//         otherData: state.otherData,
//         resolution: doubleRes(state.resolution)
//     }
// })(App);

// This is because the doubleRes function will always return a new resolution object with a new identity. 

// Reselect memoizes the last result of the function and returns it when called until new arguments are passed to it.
// import {createSelector} from “reselect”;
// const doubleRes = createSelector(
//   r => r.width,
//   r => r.height,
//   (width, height) => ({
//     width: width*2,
//     height: heiht*2
//   })
// );


// The difference between them is that React.Component doesn’t implement shouldComponentUpdate(), but React.PureComponent implements it with a shallow prop and state comparison.

//use immutable library , https://facebook.github.io/immutable-js/



//myths https://moduscreate.com/blog/react_component_rendering_performance/   comparison



_onPress = () => {
  same thing but not inline
};

render () {
  return <Touchable onPress={this._handlePress}/>
}