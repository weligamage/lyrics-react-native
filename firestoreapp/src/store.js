import { createStore, applyMiddleware, } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger} from 'redux-logger';

import AppReducer from './reducers';

const middleware = applyMiddleware( thunkMiddleware, createLogger());
const store = createStore(AppReducer, middleware);

export default store;
