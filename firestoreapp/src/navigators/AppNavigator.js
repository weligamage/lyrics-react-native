import { TabNavigator, StackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';

import ArtistsScreen from '../components/ArtistsScreen';
import SongsScreen from '../components/SongsScreen';
import LyricsScreen from '../components/LyricsScreen';
import SplashScreen from '../components/SplashScreen';

import { constants, } from '../styles';

const Nav = TabNavigator(
    {
        ArtistsTab: {
            screen: ArtistsScreen,
        },
        SongsTab: {
            screen: SongsScreen,
        }
    },
    {
        lazy: true,
        tabBarPosition: 'bottom',
        swipeEnabled: true,
        animationEnabled: false,
        tabBarOptions: {
            showLabel: true,
            showIcon: true,
            activeTintColor: constants.blue,
            activeBackgroundColor: constants.dark_2,
            inactiveTintColor: constants.dark_6,
            inactiveBackgroundColor: constants.dark_4,

            labelStyle: {
                fontSize: 12,
                fontWeight: 'bold',
            },
            style: {
                backgroundColor: constants.dark_6
            }
        },
    }
);

const AppNavigator = StackNavigator(
    {
        Splash: {
            screen: SplashScreen,
        },
        Home: {
            screen: Nav,
        },
        Lyrics: {
            screen: LyricsScreen,
        }
    },
    {
        headerMode: 'screen',
        initialRouteName: 'Splash',
        navigationOptions: {
            gesturesEnabled: false
        }
    }
);
export default AppNavigator;


