import React, { Component } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addNavigationHelpers } from 'react-navigation';

import AppNavigator from './AppNavigator';
import { commonStyles } from '../styles';

class AppWithNavigationState extends Component {

    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        nav: PropTypes.object.isRequired,
    };

    render() {
        const { nav, dispatch } = this.props;

        return (
            <View style={commonStyles.container}>
                <AppNavigator navigation={addNavigationHelpers({ dispatch, state: nav })} screenProps={ this.props.db.selectedArtist } />
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    nav: state.nav,
    db: state.db
});

export default connect(mapStateToProps)(AppWithNavigationState);
