import { StyleSheet } from 'react-native';

const constants = {
    // grey_1: '#1e2228', // inside dark
    // grey_2: '#292b2f', //tab active dark
    // grey_5: '#4f5b6b', //outside light
    // grey_7: '#86939d', //text color
    // grey_8: '#5f686f', //text bit light
    // grey_9: '#bbbbbb', //list border color,

    dark_1: "#262d36",
    dark_2: "#1f242a",
    dark_3: "#2f3640",
    dark_4: "#2f3640",
    dark_5: "#cbcbcd",
    dark_6: '#4f5b6b', //outside light


    //262d36, 1f242a, 2f3640, 2f3640, cbcbcd

    blue: '#177efb',
    tabIconSize: 26,
};

const commonStyles = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerStyle: {
        backgroundColor: constants.dark_1,
    },
    headerTitleStyle: {
        color: 'white',
        fontFamily: 'Iskoola Pota',
        fontSize: 24
    }
});

export { commonStyles, constants }; 