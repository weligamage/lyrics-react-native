import { NavigationActions } from 'react-navigation';

import AppNavigator from '../navigators/AppNavigator';


//{type: "Navigation/NAVIGATE", routeName: "Main"}
const firstAction = AppNavigator.router.getActionForPathAndParams("Splash");
//{index: 0, routes: Array(1)}
const firstNavState = AppNavigator.router.getStateForAction(firstAction);



// console.log(firstAction);
// console.log(firstNavState);

// const secondAction = AppNavigator.router.getActionForPathAndParams('ArtistsTab');
// const secondNavState = AppNavigator.router.getStateForAction(
//     secondAction,
//     firstNavState
// );


const nav = (state = firstNavState, action) => {
    let nextState;

    if (action.type === 'SHOW_ARTISTS'){
        nextState = AppNavigator.router.getStateForAction(
            NavigationActions.navigate({ routeName: 'ArtistsTab' }),
            state
        );
    }else if (action.type === 'SHOW_SONGS'){
        nextState = AppNavigator.router.getStateForAction(
            NavigationActions.navigate({ routeName: 'SongsTab', params: action.payload }),
            state
        );
    }else if (action.type === 'SHOW_LYRICS'){
        nextState = AppNavigator.router.getStateForAction(
            NavigationActions.navigate({ routeName: 'Lyrics', params: action.payload }),
            state
        );
    } else{
        nextState = AppNavigator.router.getStateForAction(action, state);
    }

    console.log(action , '==========', (nextState || state));
    // Simply return the original `state` if `nextState` is null or undefined.
    return nextState || state;
};

export default nav;



// //{type: "Navigation/NAVIGATE", routeName: "Login"}
// const secondAction = AppNavigator.router.getActionForPathAndParams('Login');
// //{index: 1, routes: Array(2)}
// const secondNavState = AppNavigator.router.getStateForAction(
//     secondAction,
//     firstNavState
// );


//action -->{type: "Navigation/NAVIGATE", routeName: "SongsTab"}
// state before moving to songs --> 
// {
//     routes: 2 [
//         {key: "ArtistsTab", routeName: "ArtistsTab"},
//         {
//             index: 0, 
//             routes: 1 [
//                 {routeName: "Home", key: "Init-id-1515985216328-0"}
//             ], 
//             key: "SongsTab", 
//             routeName: "SongsTab"}
//     ], 
//     index: 0
// }

// state after moving to songs --> 
// {
//     routes: 2 [
//         {key: "ArtistsTab", routeName: "ArtistsTab"},
//         {
//             index: 0, 
//             routes: 1 [
//                 {routeName: "Home", key: "Init-id-1515985216328-0"}
//             ], 
//             key: "SongsTab", 
//             routeName: "SongsTab"}
//     ], 
//     index: 0
// }

// state after moving to lyrics --> 
// {
//     routes: 2 [
//         {key: "ArtistsTab", routeName: "ArtistsTab"},
//         {
//             index: 0, 
//             routes: 2 [
//                 {routeName: "Home", key: "Init-id-1515985216328-0"},
//                 {params: undefined, key: "id-1515985216328-1", routeName: "Lyrics"}
//             ], 
//             key: "SongsTab", 
//             routeName: "SongsTab"}
//     ], 
//     index: 0
// }