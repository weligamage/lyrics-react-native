import { combineReducers } from 'redux';
import { NavigationActions } from 'react-navigation';

import nav from './nav';
import db from './db';

const AppReducer = combineReducers({
    db,
    nav,
});

export default AppReducer;
