const initialState = {

    isFetchingArtists: false,
    isFetchedArtists: false,
    lastFetchedArtists: [],

    isFetchingSongs: false,
    isFetchedSongs: false,
    lastFetchedSongs: [],

    filteredArtists: [],
    artistSearchText: '',

    selectedArtist: null,
    filteredByArtistSongs: [],
    filteredSongs: [],
    songSearchText: '',

    artistImageMap: null,

    error: null
};

const db = (state = initialState, action) => {

    if (action.type === 'ARTISTS_FETCHING') {
        return { ...state, isFetchingArtists: true };
    } else if (action.type === 'ARTISTS_RECEIVED') {
        return { ...state, isFetchingArtists: false, isFetchedArtists: true, lastFetchedArtists: action.payload.artistList, filteredArtists: action.payload.artistList, artistImageMap: action.payload.artistImageMap };
    } else if (action.type === 'SONGS_FETCHING') {
        return { ...state, isFetchingSongs: true };
    } else if (action.type === 'SONGS_RECEIVED') {
        return { ...state, isFetchingSongs: false, isFetchedSongs: true, lastFetchedSongs: action.payload, filteredSongs: action.payload };
    } else if (action.type === 'SHOW_SONGS') {
        let filteredByArtistSongs = [];
        for (let i = 0; i < state.lastFetchedSongs.length; i++) {
            for (let j = 0; j < state.lastFetchedSongs[i].artists.length; j++) {
                if (state.lastFetchedSongs[i].artists[j] === action.payload.name_english) {
                    filteredByArtistSongs.push(state.lastFetchedSongs[i]);
                    break;
                }
            }
        }
        return { ...state, selectedArtist: action.payload, filteredByArtistSongs, filteredSongs: filteredByArtistSongs };
    } else if (action.type === 'Navigation/NAVIGATE' && action.routeName === 'ArtistsTab') {
        if (state.selectedArtist || state.songSearchText || state.artistSearchText) {
            return { ...state, artistSearchText: '', filteredArtists: state.lastFetchedArtists, selectedArtist: null, filteredByArtistSongs: [], songSearchText: '', filteredSongs: state.lastFetchedSongs };
        }
    } else if (action.type === 'FILTER_ARTIST') {
        if (!action.payload || action.payload === '') {
            return { ...state, filteredArtists: state.lastFetchedArtists, artistSearchText: action.payload };
        } else {
            let filteredArtists = state.lastFetchedArtists.filter(({ search }) => {
                return search.match(action.payload);
            });
            return { ...state, filteredArtists, artistSearchText: action.payload };
        }
    } else if (action.type === 'FILTER_SONG') {
        const consideringList = (!state.selectedArtist) ? state.lastFetchedSongs : state.filteredByArtistSongs;
        if (!action.payload || action.payload === '') {
            return { ...state, filteredSongs: consideringList, songSearchText: action.payload };
        } else {
            let filteredSongs = consideringList.filter(({ search }) => {
                return search.match(action.payload);
            });
            return { ...state, filteredSongs, songSearchText: action.payload };
        }
    } 

    return state;
};

export default db;


