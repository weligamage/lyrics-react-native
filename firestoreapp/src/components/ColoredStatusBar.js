import React, { Component } from 'react';
import { View, StyleSheet, StatusBar, Platform } from 'react-native';

import { constants, } from '../styles';

class ColoredStatusBar extends Component {

    render() {
        return (
            <View style={styles.statusbar} >
                <StatusBar barStyle="light-content" />
            </View>
        );
    }
}

export default ColoredStatusBar;

const statusbarHeight = (Platform.OS === 'ios') ? 20 : StatusBar.currentHeight;

const styles = StyleSheet.create({
    statusbar: {
        backgroundColor: constants.dark_1,
        height: statusbarHeight
    }
});

