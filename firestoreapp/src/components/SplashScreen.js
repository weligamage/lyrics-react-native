import React, { Component } from 'react';
import { ImageBackground, StyleSheet, ActivityIndicator, View, Text, } from 'react-native';
import { connect } from 'react-redux';
import firebase from 'react-native-firebase';

import { constants, commonStyles } from '../styles';
import bgImage from "../../assets/img/splash.png";

class SplashScreen extends Component {
    static navigationOptions = {
        title: 'ගී පද',
        // title: "Sindu Lyrics",
        headerStyle: commonStyles.headerStyle,
        // headerTitleStyle: {color: 'white'}
        headerTitleStyle: commonStyles.headerTitleStyle
    };

    constructor() {
        super();
        this.artistCollectionRef = firebase.firestore().collection('artists').orderBy('name_english');//
        this.songsCollectionRef = firebase.firestore().collection('songs').orderBy('name_english');
        this.unsubscribeFromArtist = null;
        this.unsubscribeFromSongs = null;
    }

    componentDidMount() {
        console.log('did mount >>>>>>>>>', this.props);
        this.props.artistsFetching();
        this.unsubscribeFromArtist = this.artistCollectionRef.onSnapshot(this.onArtistsUpdate);
        this.props.songsFetching();
        this.unsubscribeFromSongs = this.songsCollectionRef.onSnapshot(this.onSongsUpdate);
    }
    componentWillUnmount() {
        console.log('UNMOUNT ************************', this.props);
        this.unsubscribeFromDb();
    }
    unsubscribeFromDb() {
        this.unsubscribeFromArtist();
        this.unsubscribeFromSongs();
    }
    goToTabs() {
        this.unsubscribeFromDb();
        this.props.showArtists();
    }
    onArtistsUpdate = (querySnapshot) => {
        const artistList = [];
        const artistImageMap = {};
        querySnapshot.forEach((doc) => {
            const { name_english, name_sinhala, avatar } = doc.data();
            artistList.push({
                key: doc.id,
                name_english,
                name_sinhala,
                avatar,
                search: name_english.toLowerCase()
            });
            artistImageMap[name_english] = avatar;
        });
        this.props.artistsReceived({ artistList, artistImageMap });
        if (this.props.db.isFetchedSongs) {
            this.goToTabs();
        }
    }
    onSongsUpdate = (querySnapshot) => {
        const songList = [];
        querySnapshot.forEach((doc) => {
            const { name_english, name_sinhala, artists, lyrics } = doc.data();

            let splitArr = lyrics.split("@");
            let joinArr = splitArr.join("\n");

            songList.push({
                key: doc.id,
                name_english,
                name_sinhala,
                artists,
                lyrics: joinArr,
                search: name_english.toLowerCase()
            });
        });
        this.props.songsReceived(songList);
        if (this.props.db.isFetchedArtists) {
            this.goToTabs();
        }
    }
    render() {
        const { navigation, db } = this.props;

        console.log('render >>>>>>>>>', this.props);

        return (
            <ImageBackground style={styles.container} source={bgImage}>
                <ActivityIndicator size="large" color='white'/>
                <View style={styles.textContainer}>
                    <Text style={styles.plswait}>PLEASE WAIT...</Text>
                    <Text style={styles.weare}>We are preparing your app...</Text>
                </View>
            </ImageBackground>
        );
    }
}

const mapStateToProps = (state) => ({
    db: state.db
});
const mapDispatchToProps = dispatch => ({
    artistsFetching: () => dispatch({ type: 'ARTISTS_FETCHING' }),
    artistsReceived: (payload) => dispatch({ type: 'ARTISTS_RECEIVED', payload }),

    songsFetching: () => dispatch({ type: 'SONGS_FETCHING' }),
    songsReceived: (payload) => dispatch({ type: 'SONGS_RECEIVED', payload }),

    showArtists: () => dispatch({ type: 'SHOW_ARTISTS' }),
    // login: () => dispatch(NavigationActions.navigate({ routeName: 'Login' })),
});
export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    textContainer: {
        marginTop: 50,
        marginBottom: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    plswait: {
        color: 'white',
        fontSize: 16,
        fontWeight: '200'
    },
    weare: {
        color: 'white',
        fontSize: 12,
        fontWeight: '200',
        paddingTop: 10
    }
});