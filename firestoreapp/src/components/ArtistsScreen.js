import React, { Component } from 'react';
import { View, StyleSheet, FlatList, } from 'react-native';
// import Icon from 'react-native-vector-icons/FontAwesome';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { SearchBar, List, ListItem, Avatar, } from 'react-native-elements';

import { constants, commonStyles } from '../styles';
import ColoredStatusBar from './ColoredStatusBar';

class ArtistsScreen extends Component {

    static navigationOptions = {
        header: null,
        tabBarLabel: 'Artists',
        tabBarIcon: ({ tintColor }) => (
            <Icon name="artist" size={constants.tabIconSize} color={tintColor} />
        ),
    };

    constructor() {
        super();
        console.log('artist ---> constructor');
        this.renderItem = this.renderItem.bind(this);
        this.onPressArtist = this.onPressArtist.bind(this);
    }
    renderItem = ({ item }) => {
        return (
            <ListItem
                avatar={<Avatar
                    medium
                    source={{ uri: item.avatar, cache: 'force-cache' }}
                />}
                onPress={() => this.onPressArtist(item)}
                title={item.name_sinhala}
                titleStyle={styles.sinhala}
                subtitle={item.name_english}
                subtitleStyle={styles.english}
                containerStyle={styles.listItem}
                underlayColor={constants.dark_5}
            />
        );
    };
    render() {
        console.log('artist ---> render');
        
        return (
            <View style={styles.container}>
                <ColoredStatusBar />
                <SearchBar
                    ref={search => this.search = search}
                    round
                    autoCorrect={false}
                    icon={{ name: 'search' }}
                    clearIcon={{ name: 'clear' }}
                    placeholder='Search...'
                    onChangeText={this.onChangeText}
                    // onClearText={this.onClearText}
                    containerStyle={styles.searchbar}
                    value={this.props.db.artistSearchText}
                />

                <List containerStyle={styles.list}>
                    <FlatList
                        renderItem={this.renderItem}
                        data={this.props.db.filteredArtists}
                        keyExtractor={item => item.key}
                        style={styles.list}
                    />
                </List>
            </View>
        );
    }
    onChangeText = (a) => {
        this.props.filterBy(a.toLowerCase().trim(' '));
    };
    onPressArtist = (artist) => {
        this.props.goToSongs(artist);
    };
}
const mapStateToProps = (state) => ({
    db: state.db
});
const mapDispatchToProps = dispatch => ({
    filterBy: (searchText) => dispatch({ type: 'FILTER_ARTIST', payload: searchText }),
    goToSongs: (artist) => dispatch({ type: 'SHOW_SONGS', payload: artist }),
});

export default connect(mapStateToProps, mapDispatchToProps)(ArtistsScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: constants.dark_2
    },
    searchbar: {
        borderTopWidth: 0,
        borderBottomWidth: 0,
        backgroundColor: constants.dark_1,
    },
    list: {
        marginTop: 0,
        backgroundColor: constants.dark_2,
        borderTopWidth: 0,
        borderBottomWidth: 0,
        marginBottom: 22,
        flex: 1
    },
    listItem: {
        borderBottomColor: constants.dark_3,
    },
    sinhala: {
        fontSize: 24,
        fontWeight: 'bold',
        color: 'white',
        fontFamily: 'Iskoola Pota'
    },
    english: {
        // backgroundColor: 'yellow',
        // alignItems: 'center',
        // justifyContent: 'center',
        textAlign: 'right',
        fontSize: 12,
        fontWeight: '200',
    },
});