import React, { Component } from 'react';
import { View, StyleSheet, FlatList, } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import { SearchBar, List, ListItem, Avatar, } from 'react-native-elements';

import { constants, commonStyles } from '../styles';
import ColoredStatusBar from './ColoredStatusBar';

class SongsScreen extends Component {

    static navigationOptions = ({ screenProps }) => ({
        header: null,
        tabBarLabel: (screenProps && screenProps.name_sinhala ? screenProps.name_sinhala : 'Songs'),
        tabBarIcon: ({ tintColor }) => (
            <Icon name={ (screenProps && screenProps.name_sinhala) ? "bookmark-music" : "library-music"} size={constants.tabIconSize} color={tintColor} />
        ),
    });

    constructor() {
        super();
        console.log('song ---> constructor');
        this.renderItem = this.renderItem.bind(this);
        this.onPressSong = this.onPressSong.bind(this);
    }
    renderItem = ({ item }) => {
        return (
            <ListItem
                avatar={<Avatar
                    medium
                    icon={{ name: 'music', color: constants.blue, type: 'font-awesome' }}
                    activeOpacity={0.7}
                />}
                onPress={() => this.onPressSong(item)}
                title={item.name_sinhala}
                titleStyle={styles.sinhala}
                subtitle={item.name_english}
                subtitleStyle={styles.english}
                containerStyle={styles.listItem}
                underlayColor={constants.dark_5}
            />
        );
    };
    render() {
        console.log('song ---> render', this.props);

        return (
            <View style={styles.container}>
                <ColoredStatusBar />
                <SearchBar
                    ref={search => this.search = search}
                    round
                    autoCorrect={false}
                    icon={{ name: 'search' }}
                    clearIcon={{ name: 'clear' }}
                    placeholder='Search...'
                    onChangeText={this.onChangeText}
                    // onClearText={this.onClearText}
                    containerStyle={styles.searchbar}
                    value={this.props.db.songSearchText}
                />

                <List containerStyle={styles.list}>
                    <FlatList
                        renderItem={this.renderItem}
                        data={this.props.db.filteredSongs}
                        keyExtractor={item => item.key}
                        style={styles.list}
                    />
                </List>
            </View>
        );
    }
    onChangeText = (a) => {
        this.props.filterBy(a.toLowerCase().trim(' '));
    };
    onPressSong = (song) => {
        this.props.goToLyrics(song);
    };
}

const mapStateToProps = (state) => ({
    db: state.db
});
const mapDispatchToProps = dispatch => ({
    filterBy: (searchText) => dispatch({ type: 'FILTER_SONG', payload: searchText }),
    goToLyrics: (song) => dispatch({ type: 'SHOW_LYRICS', payload: song }),
});

export default connect(mapStateToProps, mapDispatchToProps)(SongsScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: constants.dark_2
    },
    searchbar: {
        borderTopWidth: 0,
        borderBottomWidth: 0,
        backgroundColor: constants.dark_1,
    },
    list: {
        marginTop: 0,
        backgroundColor: constants.dark_2,
        borderTopWidth: 0,
        borderBottomWidth: 0,
        marginBottom: 22,
        flex: 1
    },
    listItem: {
        borderBottomColor: constants.dark_3,
    },
    sinhala: {
        fontSize: 24,
        fontWeight: 'bold',
        color: 'white',
        fontFamily: 'Iskoola Pota'
    },
    english: {
        textAlign: 'right',
        fontSize: 12,
        fontWeight: '200',
    },
    icon: {
        width: 26,
        height: 26,
    },
});