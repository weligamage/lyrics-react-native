import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View, } from 'react-native';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation';
import { Slider, Avatar, } from 'react-native-elements';

import { constants, commonStyles } from '../styles';

class LyricsScreen extends Component {

    static navigationOptions = ({ navigation, screenProps }) => ({
        title: navigation.state.params.name_sinhala,
        headerStyle: commonStyles.headerStyle,
        headerTitleStyle: commonStyles.headerTitleStyle
    });

    constructor() {
        super();
        this.state = {
            value: 23
        };
    }

    render() {
        console.log('>>>>>>', this.props);

        const artists = this.props.navigation.state.params.artists;
        const artistImageMap = this.props.db.artistImageMap;

        return (
            <View style={styles.container}>

                <ScrollView contentContainerStyle={styles.textContainer}>
                    <Text style={{ color: 'white', fontFamily: 'Iskoola Pota', fontSize: this.state.value, }}>{this.props.navigation.state.params.lyrics}</Text>
                    <View style={styles.artistContainer}>
                        {
                            artists.map((item) => (
                                <Avatar
                                    key={item}
                                    medium 
                                    source={{ uri: artistImageMap[item] }}
                                    activeOpacity={0.7}
                                />
                            ))
                        }
                    </View>
                </ScrollView>


                <View style={styles.sliderContainer}>
                    <Text style={styles.lbl}>Font Size</Text>
                    <Slider
                        value={this.state.value}
                        onValueChange={(value) => {
                            if (value !== this.state.value) {
                                this.setState({ value });
                            }
                        }}
                        maximumValue={35}
                        minimumValue={20}
                        step={1}
                        style={styles.slider}
                        thumbStyle={styles.pin}
                    />
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    db: state.db
});
const mapDispatchToProps = dispatch => ({
    goBack: () => dispatch({ type: 'BACK_FROM_LYRICS' }),
});
export default connect(mapStateToProps, mapDispatchToProps)(LyricsScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: constants.dark_2,
        justifyContent: 'space-between',
    },
    textContainer: {
        flex: 0,
        padding: 5,
        justifyContent: 'space-between'
    },
    sliderContainer: {
        flexDirection: 'row',
        height: 40,
        marginLeft: 10,
        marginRight: 10,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    lbl: {
        color: constants.dark_5,
        fontSize: 15,
        width: 80,
    },
    slider: {
        flex: 1,
    },
    pin: {
        backgroundColor: constants.blue
    },
    artistContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 30
    },
});